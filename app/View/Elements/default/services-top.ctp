<section class="Services-banner parallax-header">
	<div class="Services-bannerImage">
		<div class="points"></div>
	</div>
	<figure class="Services-bannerLogo">
		<div class="info">
			<span class="line1">Respuesta</span>
			<br/>
			<span class="line2">Inmediata</span>
			<br/>
			<span class="line3">Ante Cualquier Circunstancia</span>
		</div>
		<?= $this->Html->image('servicios/servicio-header-fixed-escudo.png'); ?>
	</figure>
	<figure class="Services-bannerAgent">
		<?= $this->Html->image('servicios/servicio-header-fixed-agente.png'); ?>
	</figure>
</section>

<div class="parallax-wrapper">

<?php if (!empty($promociones)): ?>
	<div class="Services-offers">
		<div class="container">
			<div class="Services-offersTitle">
				<div class="left">
					<span class="line1">Promociones</span>
					<br/>
					<span class="line2">Y Ofertas de Seguridad</span>
				</div>
				<div class="right">
					<span class="line1">Válido hasta</span>
					<br/>
					<span class="line2"><?= $promociones['WpPost'][0]['post_title']; ?></span>
				</div>
			</div>
			<div class="Services-offersContent">
				<?= $promociones['WpPost'][0]['post_content']; ?>
				<?php /* ?>
				<div class="col-md-4 col-sm-12" style="overflow: hidden;margin-top: 4rem;padding: 0 26px; border-right: 2px solid #000;">
					<img src="/servipro/img/servicios/imageServices.jpg" style="float:right;" alt="">					
					<p>Alarma de Seguridad<br>Paquete 01</p>
					<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
				<div class="col-md-4 col-sm-12" style="overflow: hidden;margin-top: 4rem;padding: 0 26px; border-right: 2px solid #000;">
					<img src="/servipro/img/servicios/imageServices.jpg" style="float:right;" alt="">					
					<p>Alarma de Seguridad<br>Paquete 01</p>
					<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
				<div class="col-md-4 col-sm-12" style="overflow: hidden;margin-top: 4rem;padding: 0 26px;">
					<img src="/servipro/img/servicios/imageServices.jpg" style="float:right;" alt="">					
					<p>Alarma de Seguridad<br>Paquete 01</p>
					<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
				<?php */ ?>
			</div>
		</div>
	</div>
<?php endif ?>
<div class="Services-segFis" id="security">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-offset-0 col-md-3 col-sm-offset-2 col-sm-2">
						<?= $this->Html->image('servicios/icon-gorra.gif'); ?>
					</div>
					<div class="col-md-9 col-sm-6">
						<div class="title">
							<span>Seguridad</span>
							<br/>
							<span>Física</span>
						</div>
						<div class="content">
							<p>Son aquellas medidas que se toman para prevenir el acceso fisico a la entrada de personas no autorizadas a una instalación o área protegida.</p>
							<p>En Servipro garantizamos una vigilancia segura y profesional. Nuestro personal es altamente calificado y cumple o excede los estandares del país.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 image">
				<?= $this->Html->image('servicios/seguridad-confiar.jpg'); ?>
			</div>
		</div>
	</div>
</div>

</div>