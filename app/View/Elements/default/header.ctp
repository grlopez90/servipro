<section class="MainMenu-wrapper">
	<div class="container">
		<nav class="navbar navbar-default">
		    <div class="container-fluid">
		        <!-- Brand and toggle get grouped for better mobile display -->
		        <div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand" href="/">
		            	<?= $this->Html->image('img-logo01.png'); ?>
		            	<span>SERVIPRO</span>
		            </a>
		        </div>
		        <!-- Collect the nav links, forms, and other content for toggling -->
		        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		            <ul class="nav navbar-nav navbar-right">
		                <li class="<?= ($this->request['controller'] == 'homes')?'active':'' ?>"><a href="<?=$this->Html->url('/')?>">INICIO</a></li>
		                <li class="<?= ($this->request['controller'] == 'servicio')?'active':'' ?>"><a href="<?=$this->Html->url('/servicio')?>">SERVICIOS</a></li>
		                <li class="<?= ($this->request['controller'] == 'nuestraempresa')?'active':'' ?>"><a href="<?=$this->Html->url('/nuestraempresa')?>">NUESTRA EMPRESA</a></li>
		                <li class="<?= ($this->request['controller'] == 'posts')?'active':'' ?>"><a href="<?=$this->Html->url('/posts?c=2')?>">NOTICIAS</a></li>
		                <li><a href="<?=$this->Html->url('/homes#Contact')?>">CONTACTO</a></li>
		            </ul>
		        </div>
		        <!-- /.navbar-collapse -->
		    </div>
		    <!-- /.container-fluid -->
		</nav>
	</div>
	<aside class="extraLinks">
		<nav>
			<ul>
				<li>
					<a href="http://161.0.34.148:91/kronosWebApplication/LogIn.aspx" style="height: 34px;">
						<?= $this->Html->image('img-user.png',["style"=>"float:left;margin-top:5px;"]); ?> 
						<span style="font-size: 10px;float: left;margin-left: 10px;">
							TU CUENTA <br> SERVIPRO
						</span>
					</a>
				</li>
				<li>
					<a href="" style="height: 34px;">
						<?= $this->Html->image('img-pistola.png',["style"=>"float:left;margin-top:5px;"]); ?>
						<span style="font-size: 10px;float: left;margin-left: 10px;">
							VENTA DE <br> ARMAS
						</span>
					</a>
				</li>
			</ul>
		</nav>
	</aside>
</section>