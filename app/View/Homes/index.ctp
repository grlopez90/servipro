<section class="MainSection">
    <?php if (!empty($slider)): ?>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php $cont=0; ?>
                <?php foreach ($slider[0]['WpPost'] as $foto): ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?= $cont; ?>" class="<?= ($cont==0)?'active':''; ?>"></li>
                    <?php $cont++; ?>
                <?php endforeach ?>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php $cont=0; ?>
                <?php foreach ($slider[0]['WpPost'] as $foto): ?>
                    <div class="item <?= ($cont==0)?'active':''; ?>">
                        <?= $foto['post_content']; ?>
                        <div class="carousel-caption">
                            ...
                        </div>
                    </div>    
                    <?php $cont++; ?>
                <?php endforeach ?>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="points"></div>
    <?php endif ?>
</section>

<div class="HomeServicios">
    <div class="container">
        <div class="row">
            <div class="hidden-lg col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12 HomeServicios-logo-md">
                        <?= $this->Html->image('img-solo-logo.png'); ?>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <a href="<?= $this->Html->url('/servicio#security'); ?>" class="HomeServicios-item-md">SEGURIDAD FISICA</a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <a href="<?= $this->Html->url('/servicio#ElectronicSecurity'); ?>" class="HomeServicios-item-md">SEGURIDAD ELECTRONICA</a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <a href="<?= $this->Html->url('/servicio#profile'); ?>" class="HomeServicios-item-md">CONSULTORIAS/SERV. ESPECIALES</a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <a href="<?= $this->Html->url('/servicio#messages'); ?>" class="HomeServicios-item-md">CAPACITACIONES</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 hidden-md hidden-sm hidden-xs">
                <div class="HomeServicios-listWrapper">
                    <ul>
                        <li>
                            <a href="<?= $this->Html->url('/servicio#security'); ?>">SEGURIDAD FISICA</a>
                        </li>
                        <li>
                            <a href="<?= $this->Html->url('/servicio#ElectronicSecurity'); ?>">SEGURIDAD ELECTRONICA</a>
                        </li>
                        <li>
                            <a href="<?= $this->Html->url('/servicio#messages'); ?>">CAPACITACIONES</a>
                        </li>
                        <li>
                            <a href="<?= $this->Html->url('/servicio#profile'); ?>">CONSULTORIAS/SERV. ESPECIALES</a>
                        </li>
                        <a href="" class="HomeServicios-logo">
                            <?= $this->Html->image('sombra-placa.png', array('style'=>'top:34px;')); ?>
                            <?= $this->Html->image('img-solo-logo.png'); ?>
                        </a>
                    </ul>
                </div>
                <div class="HomeServicios-itemWrapper">
                    <div class="row">
                        <div class="col-md-2" style="margin-left: 4rem">
                            <?= $this->Html->image('servicios/img-seguridad-fisica.jpg',['url'=>'/servicio#security']); ?>
                        </div>
                        <div class="col-md-2" style="margin-left: 2rem">
                            <?= $this->Html->image('servicios/img-seguridad-electronica.jpg',['url'=>'/servicio#ElectronicSecurity']); ?>
                        </div>
                        <div class="col-md-2 HomeServicios-itemSolution">
                            SOLUCIONES INTELIGENTES <br>
                            Para tu Seguridad
                        </div>
                        <div class="col-md-2" style="margin-left: 3rem">
                            <?= $this->Html->image('servicios/img-consultorias.jpg',['url'=>'/servicio#messages']); ?>
                        </div>
                        <div class="col-md-2" style="margin-left: 3rem">
                            <?= $this->Html->image('servicios/img-capacitaciones.jpg',['url'=>'/servicio#profile']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($videos)): ?>
    <div class="HomeVideos">
        <div class="container">
            <?= $videos['WpPost'][0]['post_content']; ?>
        </div>
    </div>
<?php endif ?>

<div class="Articulos">
    <header class="Articulos-header">
        <div class="container">
            <h3>
                NOTICIAS <br>
            </h3>
            <span>LO MAS RECIENTE</span>
        </div>
    </header>
    <section class="Articulos-body">
        <div class="container">
            <div class="row">
                <?php foreach ($articulos[0]['WpPost'] as $post): ?>
                    <div class="col-md-4 Articulo-item">
                        <div class="row">
                            <a href="<?= $this->Html->url(array('controller'=>'posts','action'=>'view','title' => $post['ID'].'-'.$post['post_name'])); ?>" style="color:#000">
                                <div class="col-md-6">
                                    <h4><?= $post['post_title'] ?></h4>
                                    <?php $contenido = strip_tags($post['post_content'],'<p>'); ?>
                                    <?= $this->Text->truncate($contenido,220); ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="Articulo-itemPicture">
                                        <?= $this->Html->image($post['Image']['WpPost']['guid']); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </section>
    <footer class="Articulos-footer">
        
    </footer>
</div>

<div class="Contact" id="Contact">
    <a href="" class="btn Contact-btnAgent Contact-btnAgentBack">
        <?= $this->Html->image('btn-back-02.jpg'); ?>
    </a>
    <div class="Contact-bgOver">
        <?= $this->Html->image('agentes-00.png', ['class'=>'Contact-agent hidden-md hidden-sm hidden-xs']); ?>
        <a href="" class="btn Contact-btnAgent hidden-md hidden-sm hidden-xs">
            <?= $this->Html->image('btn-sedes-02.png'); ?>
        </a>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-md-offset-6 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <?= $this->Html->image('img-solo-logo.png'); ?>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 Contact-info">
                            <p>COMUNIQUESE CON NOSOTROS PARA MAS INFORMACION, NOS GUSTARIA AYUDARLE CON SU SEGURIDAD.</p>
                            <p>
                                NUESTRO PERSONAL DE SERVICIO AL CLIENTE ESTA ESPERANDO SU LLAMADA.
                            </p>
                            <div class="pull-right Contact-infoNumbers">
                                <?= $this->Html->image('img-call.png'); ?>
                                (505) 2255 8800 <br>
                                (505) 2267 8118
                            </div>
                        </div>
                    </div>
                    <div class="Contact-form">
                        <?php echo $this->Form->create('contact',['url'=>'/Nuestraempresa/contacto']); ?>
                            <label for="">TU NOMBRE</label>
                            <input type="text" required="required" name="data[Contact][Name]" class="form-control">
                            <label for="">CORREO ELECTRONICO</label>
                            <input type="email" name="data[Contact][Email]" required="required" class="form-control">
                            <label for="">MENSAJE</label>
                            <textarea name="data[Contact][Message]" required="required" id="" class="form-control"></textarea>
                            <!-- <input type="submit" name="send" value="ENVIAR" class="btn pull-right Contact-btnSend"> -->
                        <?php echo $this->Form->end(array('label' => 'ENVIAR','class' => 'btn pull-right Contact-btnSend')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>