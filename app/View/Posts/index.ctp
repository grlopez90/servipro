<section class="Articles">
	<div class="Articles-top parallax-header">
		<div class="Articles-search">
			<div class="Articles-searchInfo">
				<span class="line1">Siempre Activos</span>
				<br/>
				<span class="line2">Llegamos a ser</span>
				<br/>
				<span class="line3">Los Mejores</span>
			</div>
			<div class="Articles-searchForm">
				<form method="get" action="<?php echo $this->Html->url(array('controller'=>'posts','action'=>'index?c=2')); ?>">
					<input type="hidden" name="c" id="c" class="input" value="2">
					<input type="text" name="keyword" id="keyword" class="input">
					<input type="submit" value="" class="button">
				</form>
				<div class="label">
					Buscar Artículos
				</div>
			</div>
		</div>
		<div class="Articles-topPoints"></div>
	</div>
	<div class="parallax-wrapper">
		<div class="Articles-list">
			<div class="container">
				<?php $year = CakeTime::format('Y', time()); ?>
				<?php foreach ($articulos as $post): ?>
					<?php if (CakeTime::format('Y', $post['WpPost']['post_date']) != $year): ?>
						<?php $year = CakeTime::format('Y', $post['WpPost']['post_date']); ?>
						<div class="timeLine">
							<span><?= CakeTime::format('Y', $post['WpPost']['post_date']) ?></span>
						</div>
					<?php endif ?>
					<article>
						<div class="right">
							<?php if (!empty($post['Image'])): ?>
								<div class="image">
									<?= $this->Html->image($post['Image']['WpPost']['guid']); ?>
								</div>
							<?php endif ?>
						</div>
						<div class="left">
							<div class="header">
								<h2><?= $post['WpPost']['post_title']; ?></h2>
								<span class="date">
									Subido <?= CakeTime::format('d/m/Y', $post['WpPost']['post_date']); ?>
								</span>
							</div>
							<div class="content">
								<?php $contenido = strip_tags($post['WpPost']['post_content'],'<p>'); ?>
								<?= $this->Text->truncate($contenido,550); ?>
							</div>
							<?= $this->Html->link(
								'Leer Mas...', 
								array(
									'controller'=>'posts',
									'action'=>'view',
									'title' => $post['WpPost']['ID'].'-'.$post['WpPost']['post_name']
								), 
								array('class'=>'button')
							); ?>
						</div>
					</article>
				<?php endforeach ?>
			</div>
		</div>
		<div class="pagination">
			<div class="container">
				<?php
				if ($category != '') {
		        	$options['url'] = array_merge($this->passedArgs, array('?'=> 'c='.$category)); 
		        	$this->paginator->options($options); 	             
		        }
		        if ($keyword != '') {
		        	$options['url'] = array_merge($this->passedArgs, array('?'=> 'c=2&keyword='.$keyword)); 
		        	$this->paginator->options($options); 	             
		        }
				echo $this->paginator->prev('« Anterior', null, null, array('class' => 'disabled prev'));
			    echo $this->paginator->numbers(
			    	array(
			    		'separator'=>'',
			    		'tag'=>'span',
			    		'class'=>'numbers'
			    	)
			    ); 
			    echo $this->paginator->next('Siguiente »', null, null, array('class' => 'disabled next')); 
			    echo $this->paginator->counter(
			    	array(
			    		'format'=>'<span class="counter">%page% de %pages% paginas</span>'
			    	)
			    );
			    ?>
			</div>
		</div>
	</div>
</section>

