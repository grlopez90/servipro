<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 */
class PostsController extends AppController {

	public $components = array('Paginator', 'Funciones');

	public function index() {

		$title_for_layout = 'Error 404';

		if (isset($_GET['c']) and !empty($_GET['c'])) {

			$term_taxonomy_id = $_GET['c'];
			
			$this->loadModel('WpPost');

			$this->paginate = array(
				'conditions' => array(
					'WpTermRelationship.term_taxonomy_id' => $term_taxonomy_id,
					'WpPost.post_status' => 'publish'
				),
				'order' => 'post_date DESC',
		        'limit' => 5,
		    );
		    
			if (isset($_GET['keyword']) and !empty($_GET['keyword'])) {

				$permalink = $this->Funciones->generatePermalink($_GET['keyword']);
	        	$pclave = str_replace('-', ' ', $_GET['keyword']);
	        	$articulos = $this->paginate(
	        		'WpPost',
	        		 array(
	                    'MATCH(WpPost.post_title) AGAINST ("' . $pclave . '" IN BOOLEAN MODE)'
	                )
	        	);

			} else {
		    	$articulos = $this->paginate('WpPost');
			}

		    for ($i=0; $i < 5; $i++) { 
		    	if (!empty($articulos[$i]['WpPostmetum'])) {
			    	$articulos[$i]['Image'] = $this->WpPost->find(
						'first',
						array(
							'conditions' => array(
								'WpPost.ID' => $articulos[$i]['WpPostmetum']['meta_value'],
							),
							'fields' => array('guid')
						)
					);
		    	}
		    }

			$title_for_layout = 'Noticias';
		}

		$this->set(
			array(
				'title_for_layout' => @$title_for_layout,
				'articulos' => @$articulos,
				'category' => 2,
				'keyword' => @$permalink
			)
		);
	}

	public function view() {

		$title_for_layout = 'Error 404';

		if (isset($this->passedArgs['title']) and !empty($this->passedArgs['title'])) {
			$permalink = explode('-', $this->passedArgs['title']);
			$id = $permalink[0];

			$this->loadModel('WpPost');
			$articulo = $this->WpPost->find(
				'first',
				array(
					'conditions' => array(
						'WpPost.ID' => $id,
					)
				)
			);

			if (!empty($articulo)) {
				$title_for_layout = $articulo['WpPost']['post_title'];
				$articulo['Image'] = $this->WpPost->find(
					'first',
					array(
						'conditions' => array(
							'WpPost.ID' => $articulo['WpPostmetum']['meta_value'],
							//'Articulo.estado' => 1
						)
					)
				);

				$this->loadModel('WpTermTaxonomy');
				$this->WpTermTaxonomy->unbindModel(array('hasAndBelongsToMany' => array('WpPost')),true);
				$this->WpTermTaxonomy->bindModel(
					array(
						'hasAndBelongsToMany' => array(
							'WpPost' => array(
								'className' => 'WpPost',
								'joinTable' => 'wp_term_relationships',
								'foreignKey' => 'term_taxonomy_id',
								'associationForeignKey' => 'object_id',
								'unique' => 'keepExisting',
								'conditions' => array(
									'WpPost.post_status' => 'publish',
									'WpPost.ID < ' => $articulo['WpPost']['ID']

									),
								'fields' => array('WpPost.ID', 'WpPost.post_name'),
								'order' => 'post_date DESC',
								'limit' => 1,
							)
						)
					)
				);
				$articulo['Prev'] = $this->WpTermTaxonomy->find(
					'first',
					array(
						'conditions' => array(
							'slug' => 'articulos',
						)
					)
				);
				$this->WpTermTaxonomy->bindModel(
					array(
						'hasAndBelongsToMany' => array(
							'WpPost' => array(
								'className' => 'WpPost',
								'joinTable' => 'wp_term_relationships',
								'foreignKey' => 'term_taxonomy_id',
								'associationForeignKey' => 'object_id',
								'unique' => 'keepExisting',
								'conditions' => array(
									'WpPost.post_status' => 'publish',
									'WpPost.ID > ' => $articulo['WpPost']['ID']

									),
								'fields' => array('WpPost.ID', 'WpPost.post_name'),
								'order' => 'post_date ASC',
								'limit' => 1,
							)
						)
					)
				);
				$articulo['Next'] = $this->WpTermTaxonomy->find(
					'first',
					array(
						'conditions' => array(
							'slug' => 'articulos',
						)
					)
				);
			}
		}
		$this->set(compact('title_for_layout' ,'articulo'));
	}

}
