<?php
App::uses('AppController', 'Controller');
/**
 * Services Controller
 */
class ServicesController extends AppController {

	public function index() {
		$this->loadModel('WpTermTaxonomy');
		$promociones = $this->WpTermTaxonomy->find(
			'first',
			array(
				'conditions' => array(
					'slug' => 'promociones',
				)
			)
		);
		$productos = $this->WpTermTaxonomy->find(
			'first',
			array(
				'conditions' => array(
					'slug' => 'productos',
				)
			)
		);
		$this->set(compact(array('promociones', 'productos')));
	}

}
