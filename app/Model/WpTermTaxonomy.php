<?php
App::uses('AppModel', 'Model');
/**
 * WpTermTaxonomy Model
 *
 * @property Term $Term
 * @property WpPost $WpPost
 */
class WpTermTaxonomy extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'wp_term_taxonomy';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'term_taxonomy_id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'taxonomy';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'taxonomy' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'WpTerm' => array(
			'className' => 'WpTerm',
			'foreignKey' => 'term_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'WpPost' => array(
			'className' => 'WpPost',
			'joinTable' => 'wp_term_relationships',
			'foreignKey' => 'term_taxonomy_id',
			'associationForeignKey' => 'object_id',
			'unique' => 'keepExisting',
			'conditions' => array('WpPost.post_status' => 'publish'),
			'fields' => '',
			'order' => 'post_date DESC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
