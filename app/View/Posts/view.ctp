<section class="Article">
	<div class="container">
		<div class="col-md-12">
			<?php if (!empty($articulo)): ?>
				<header class="Article-header">
					<h1><?= $articulo['WpPost']['post_title']; ?></h1>
					<span class="date">Subido <?= CakeTime::format('d/m/Y', $articulo['WpPost']['post_date']); ?></span>
				</header>
				<?php if (!empty($articulo['Image'])): ?>
					<div class="Article-image">
						<?= $this->Html->Image($articulo['Image']['WpPost']['guid']); ?>
					</div>
				<?php endif ?>
				<div class="Article-content">
					<div class="row">
						<?= $articulo['WpPost']['post_content']; ?>
					</div>
				</div>
				<div class="Article-links">
					<?php 
					$url = '';
					$class = '';
					if (!empty($articulo['Prev']['WpPost'])) {
						$url = array(
							'controller' => 'posts',
							'action' => 'view',
							'title' => $articulo['Prev']['WpPost'][0]['ID'].'-'.$articulo['Prev']['WpPost'][0]['post_name']

						);
						$attr = array('class' => 'button');
					} else {
						$attr = array('class' => 'button disabled');
					} 
					?>
					<?= $this->Html->link('« Nota Anterior', $url, $attr) ?>
					<?php 
					$url = '';
					$class = '';
					if (!empty($articulo['Next']['WpPost'])) {
						$url = array(
							'controller' => 'posts',
							'action' => 'view',
							'title' => $articulo['Next']['WpPost'][0]['ID'].'-'.$articulo['Next']['WpPost'][0]['post_name']

						);
						$attr = array('class' => 'button');
					} else {
						$attr = array('class' => 'button disabled');
					} 
					?>
					<?= $this->Html->link('Nota Siguiente »', $url, $attr) ?>
				</div>
			<?php endif ?>
		</div>
	</div>
</section>