<div class="AboutMain parallax-header">
	<div class="points"></div>
</div>

<div class="parallax-wrapper">

	<div class="AboutDescription">
		<div class="container">
			<div class="row">
				<div class="col-md-12 AboutDescription-title">
					<h1>Somos pioneros en la seguridad privada en Nicaragua</h1>
				</div>
				<div class="col-md-7">
					<?= $this->Html->image('00-imagen-cambiar-placeholder.jpg',['class'=>'img-responsive']); ?>
				</div>
				<div class="col-md-5 AboutParrgraph-text">
					<p>
						Somos la primera empresa de seguridad privada establecida legalmente en Nicaragua; precursores de la introducción de sistemas y dispositivos de seguridad electrónica al país. Somos la única empresa nicaragüense de seguridad que gestiona sus procesos bajo la norma internacional de calidad ISO 9001:2008, supervisada y regulada por el Instituto Colombiano de Normas Técnicas Internacionales ICONTEC. Somos miembro de la Asociación Latinoamericana de Seguridad Electrónica, ALAS y de la Asociación de Empresas de Seguridad Privada de Nicaragua ASEGPRIN.
					</p>
					<p>
						Ofrecemos una amplia gama de productos y servicios de seguridad física, electrónica, consultorías y capacitaciones; cimentamos nuestros eficacia en más de 23 años de experiencia, lo que además nos permite, brindar soluciones integrales de seguridad adaptadas a las necesidades y capacidad financiera de los clientes.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="AboutCertification">
		<div class="container">
			<div class="row">
				<div class="col-md-12 AboutCertification-header">
					<h2 class="AboutTitle">NUESTRAS CERTIFICACIONES</h2>
					<span class="AboutSubtitle">ISO 9001-2008</span>
				</div>
				<div class="col-md-4">
					<?= $this->Html->image('nuestraempresa-certificaciones.png',['class'=>'img-responsive']); ?>
				</div>
				<div class="col-md-8 AboutParrgraph-text">
					<p>
						El sistema de gestión de calidad es una estructura operacional de trabajo, bien documentada e integrada a los procedimientos técnicos y gerenciales, para guiar la prestación del servicio, obtener los requerimientos de los clientes y manejar la información de la organización de manera práctica y coordinada, con el fin de asegurar la satisfacción del cliente.
					</p>
					<p>
						El Sistema de Gestión de la Calidad de SERVIPRO está basado en la Norma Internacional ISO 9001: 2008. Esta norma fue elaborada por la Organización Internacional para la Estandarización (ISO); que es la entidad internacional encargada de favorecer la normalización en el mundo. Con sede en Ginebra, es una federación de organismos nacionales, éstos, a su vez, son oficinas de normalización que actúan de delegadas en cada país, como por ejemplo: AENOR en España, AFNOR en Francia, DIN en Alemania, etc. con comités técnicos que llevan a término las normas. Se creó para dar más eficacia a las normas nacionales.
					</p>
					<p>
						Somos una de las primeras empresas de seguridad certificada en Nicaragua bajo esta norma y hemos mantenido esta certificación durante ocho.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="AboutWhoUs">
		<div class="container">
			<div class="row">
				<div class="col-md-12 AboutParrgraph-text">
					<h2 class="AboutTitle">SERVIPRO</h2>
					<p>
						SERVIPRO es la primera empresa de seguridad privada establecida en Nicaragua que introdujo los sistemas y dispositivos de seguridad electrónica al país, gestionada con normas internacionales de calidad supervisadas y reguladas por el Instituto Colombiano de Normas Técnicas Internacionales ICONTEC.
					</p>
					<p>
						SERVIPRO ofrece una amplia gama de productos y servicios de seguridad con valores agregados a una gran variedad de clientes individuales, corporativos e institucionales en las áreas de: Seguridad Física, Seguridad Electrónica, Consultorías y Capacitación; acompañado de más de 20 años de experiencia que le permiten brindar soluciones integrales adaptadas a las necesidades e inversión de los clientes.
					</p>
				</div>
				<!-- <div class="col-md-5">
					<?= $this->Html->image('nuestraempresa-escudo-chachas.jpg',['class'=>'img-responsive']); ?>
				</div> -->
			</div>
			<?php if (!empty($gerentes)): ?>
				<div class="row">
					<div class="col-md-12 AboutWhoUs-listProfiles">
						<?= $gerentes['WpPost'][0]['post_content']; ?>
					</div>
				</div>
			<?php endif ?>
			<div class="row">
				<div class="col-md-6 AboutParrgraph-text AboutWhoUs-info">
					<h3 class="AboutTitle">NUESTRA MISIÓN</h3>
					<p>
						Brindar soluciones de seguridad adaptadas a las demandas contemporáneas.
					</p>
					<p>
						Nuestra visión es ser el líder de las empresas de seguridad privada, con personal creativo, utilizando el conocimiento y la tecnología en una organización gestionada con normas internacionales de calidad.
					</p>
				</div>
				<div class="col-md-6 AboutParrgraph-text AboutWhoUs-info">
					<h3 class="AboutTitle">NUESTRO COMPROMISO</h3>
					<p>
						Estamos comprometidos con la satisfacción de nuestros clientes, comprender sus necesidades actuales y futuras; cumplir sus requisitos mediante procesos que garanticen la mejora continua, agreguen valor e incrementen la protección de nuestros clientes.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="AboutClients">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h2 class="AboutTitle">NUESTROS CLIENTES SON LO MAS IMPORTANTE</h2>
					<p>
						<article class="AboutParrgraph-text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore optio cumque, quae nulla libero, perspiciatis perferendis magnam odio repudiandae. Tempore eius nisi iusto tenetur delectus, nihil ipsam illo id molestias.
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore optio cumque, quae nulla libero, perspiciatis perferendis magnam odio repudiandae. Tempore eius nisi iusto tenetur delectus, nihil ipsam illo id molestias.
						</article>
					</p>
				</div>
				<div class="col-md-8" style="text-align: center;">
					<iframe style="max-width: 100%;" width="614" height="342" src="https://www.youtube.com/embed/oPlcnX0DpGc" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="col-md-12 AboutClients-title">
					<h2 class="AboutTitle">NUESTROS CLIENTES NOS RECOMIENDAN</h2>
				</div>
			</div>
		</div>
		<div class="a-wrapper">
			<ul class="AboutClients-list">
				<li><?= $this->Html->image('clientes/00-aceros-roag.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/01-aginsa.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/02-automaster.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/03-cafe-soluble.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/04-comisiarato-policia.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/05-epn.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/06-hogar-zacarias.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/07-ocal.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/08-procredit.jpg',['class'=>'img-responsive']); ?></li>
				<li><?= $this->Html->image('clientes/09-zermat.jpg',['class'=>'img-responsive']); ?></li>
			</ul>
		</div>
	</div>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-gerenteName" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body modal-gerente">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>