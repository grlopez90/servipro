<?php
App::uses('NuestraempresasController', 'Controller');

/**
 * NuestraempresasController Test Case
 */
class NuestraempresasControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.nuestraempresa'
	);

}
