<div class="Services parallax-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
			  <!-- Nav tabs -->
				<ul class="nav nav-tabs nav-tabsMine" role="tablist">
					<li role="presentation" class="active">
						<a href="#home" aria-controls="home" role="tab" data-toggle="tab">
							<?= $this->Html->image('icon-guarda-01.gif'); ?>
							<span>Vigilancia <br>Fisica </span> 
							<div class="triangle" style="display: block"></div>
						</a>
					</li>
					<li role="presentation">
						<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
							<?= $this->Html->image('icon-estrategia-01.gif'); ?>
							<span>Consultoria <br>y Servicios <br>Especiales</span> 
							<div class="triangle"></div>
						</a>
					</li>
					<li role="presentation">
						<a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
							<?= $this->Html->image('icon-capacitacion-01.gif'); ?>
							<span>Cursos y <br>Capacitaciones </span>
							<div class="triangle"></div>
						</a>
					</li>
				</ul>

			  <!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						
						<div class="col-md-3 col-xs-4 left"> <!-- required for floating -->
						    <!-- Nav tabs -->
						    <ul class="nav nav-tabs tabs-left">
						      <li class="active">
							      <a href="#home2" data-toggle="tab">
							      	<span>Vigilancia Fisica y Protecció de Instalaciones.</span>
							      	<div class="triangle2" style="display: block;"></div>
							      </a>
						      </li>
						      <li>
						      	<a href="#profile2" data-toggle="tab">
						      		<span>Protección de Personal</span>
						      		<div class="triangle2"></div>
						      	</a>
						      </li>
						      <li>
						      	<a href="#messages2" data-toggle="tab">
						      		<span>Traslado de Valores</span>
						      		<div class="triangle2"></div>
						      	</a>
						      </li>
						    </ul>
						</div>

						<div class="col-md-9 col-xs-8 right">
						    <!-- Tab panes -->
						    <div class="tab-content">
						      	<div class="tab-pane active" id="home2">
							      	<div class="tabSmall">
							      		<div class="pull-left">
									      	<p>Es el servicio de protección de bienes que ofrecemos a través de la permanencia de vigilantes o guardas de seguridad en las instalaciones de su propiedad o en su posesión legal en diferentes modalidades.</p>
									      	<p>El servicio está dirigido a los siguientes segmentos: estado, micro financieras, organismos no gubernamentales, residencial, bancos, empresas comerciales e industriales y de servicio.</p>
									      	<p>Seguridad de eventos: servicio de proyección de personas y bienes durante eventos artísticos, culturales, deportivos y sociales, proyección de personalidades, equipos, control de acceso, control de movimiento de personas, prestado mediante guardas de seguridad entrenados que desarrollan funciones y procedimientos adaptados a las particularidades del evento.</p>
							      		</div>
							      		<div class="pull-right">
							      			<div class="imageGalSmall">
							      				<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/vigilancia_fisica/f-01.jpg'),'/img/servicios/servicio_seg_fisica/vigilancia_fisica/f-01.jpg',['class'=>'group_photo','escape'=>false]); ?>
							      			</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/vigilancia_fisica/f-02.jpg'),'/img/servicios/servicio_seg_fisica/vigilancia_fisica/f-02.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/vigilancia_fisica/f-03.jpg'),'/img/servicios/servicio_seg_fisica/vigilancia_fisica/f-03.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/vigilancia_fisica/f-04.jpg'),'/img/servicios/servicio_seg_fisica/vigilancia_fisica/f-04.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/vigilancia_fisica/f-05.jpg'),'/img/servicios/servicio_seg_fisica/vigilancia_fisica/f-05.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/vigilancia_fisica/f-06.jpg'),'/img/servicios/servicio_seg_fisica/vigilancia_fisica/f-06.jpg',['class'=>'group_photo','escape'=>false]); ?>
							      			</div>
							      		</div>
							      	</div>
						      	</div>
						      	<div class="tab-pane" id="profile2">
							      	<div class="tabSmall">
								      	<div class="pull-left">
								      		<p>Es el servicio de protección de bienes que ofrecemos a través de la permanencia de vigilantes o guardas de seguridad en las instalaciones de su propiedad o en su posesión legal.El servicio está dirigido a los siguientes segmentos: Estado Microfinancieras Organismos No Gubernamentales Residencial Bancos Empresas comerciales, industriales y de servicio.</p>
								      	</div>
							      		<div class="pull-right">
							      			<div class="imageGalSmall">
							      				<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/proteccion_personal/V-01.jpg'),'/img/servicios/servicio_seg_fisica/proteccion_personal/V-01.jpg',['class'=>'group_photo','escape'=>false]); ?>
							      			</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/proteccion_personal/V-02.jpg'),'/img/servicios/servicio_seg_fisica/proteccion_personal/V-02.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/proteccion_personal/V-03.jpg'),'/img/servicios/servicio_seg_fisica/proteccion_personal/V-03.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/proteccion_personal/V-04.jpg'),'/img/servicios/servicio_seg_fisica/proteccion_personal/V-04.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/proteccion_personal/V-05.jpg'),'/img/servicios/servicio_seg_fisica/proteccion_personal/V-05.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/proteccion_personal/V-06.jpg'),'/img/servicios/servicio_seg_fisica/proteccion_personal/V-06.jpg',['class'=>'group_photo','escape'=>false]); ?>
							      			</div>
							      		</div>
							      	</div>
						      	</div>
						      	<div class="tab-pane" id="messages2">
							      	<div class="tabSmall">
								      	<div class="pull-left">
											<p>Es el transporte seguro de efectivos o valores, desde un punto de origen hasta el punto destino, realizado bajo la responsabilidad y control de SERVIPRO bajo procedimientos establecidos y verificados, empleando personal y equipos de transporte especializados.El servicio está dirigido a los siguientes segmentos: Estado Micro financieras ONG Personas Nacionales Empresas comerciales, industriales y de servicio Pymes comerciales, industriales y de servicio</p>
								      	</div>
								      	<div class="pull-right">
							      			<div class="imageGalSmall">
							      				<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/traslado_de_valores/t-01.jpg'),'/img/servicios/servicio_seg_fisica/traslado_de_valores/t-01.jpg',['class'=>'group_photo','escape'=>false]); ?>
							      			</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/traslado_de_valores/t-02.jpg'),'/img/servicios/servicio_seg_fisica/traslado_de_valores/t-02.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/traslado_de_valores/t-03.jpg'),'/img/servicios/servicio_seg_fisica/traslado_de_valores/t-03.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/traslado_de_valores/t-04.jpg'),'/img/servicios/servicio_seg_fisica/traslado_de_valores/t-04.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/traslado_de_valores/t-05.jpg'),'/img/servicios/servicio_seg_fisica/traslado_de_valores/t-05.jpg',['class'=>'group_photo','escape'=>false]); ?>
								      		</div>
							      			<div class="imageGalSmall">
								      			<?= $this->Html->link($this->Html->image('servicios/servicio_seg_fisica/traslado_de_valores/t-06.jpg'),'/img/servicios/servicio_seg_fisica/traslado_de_valores/t-06.jpg',['class'=>'group_photo','escape'=>false]); ?>
							      			</div>
							      		</div>
							      	</div>
						     	</div>
						    </div>
						</div>  



					</div>
					<div role="tabpanel" class="tab-pane" id="profile">
						<div class="col-md-5 col-xs-7 left">
							<div class="spy">
								<?php /*$this->Html->image('servicios/consultorias_y_serv_especiales/spy.jpg'); */?>
							</div>
							<ul class="nav nav-tabs tabs-left">
								<li class="active">
									<a href="#EstudioSeguridad" style="text-align: justify">
										Estudios de seguridad privada integral elaborados por expertos nacionales y extranjeros. Expone propuestas integrales de protecciÓn física y electrÓnica con polÍticas, diseÑo organizativo, procedimientos y programas de capacitaciÓn del personal que gestionará el sistema de seguridad de la entidad solicitante. Modalidades del servicio:
									</a>
								</li>
							</ul>
						</div>
						<div class="col-md-7 col-xs-5 right">		
							<div class="tab-content">
					      	<div class="tab-pane active" id="EstudioSeguridad">
						      	<div class="tabSmall">
						      		<div class="pull-left">
										<ul>
											<li>AnÁlisis y evaluaciÓn de riesgo.</li>
											<li>ElaboraciÓn de planes de seguridad.</li>
											<li>AdministraciÓn, desarrollo o implantación de panes de seguri');?></li>
											<li>Due Diligence.</li>
											<li>Comprobación y verificaciÓn de empleados.</li>
											<li>Pruebas de confiabilidad.</li>
										</ul>
						      		</div>
						      		<div class="pull-right">
						      			<?= $this->Html->image('consultorias-y-serv-especiales.png'); ?>
						      		</div>
				      			</div>
				      		</div>
					     </div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="messages">
						<div class="tabLarge">
							<div class="pull-left">
								<p>Procesos de enseñanza-aprendizaje, para el desarrollo de habilidades yezas de los prestadores de servicio de seguridad privada, que les permite mejorar el desempeño en sus labores habituales</p>
								<ul>
									<li>Cursos básicos y/o especializados de guardas de seguridad.</li>
									<li>Curso de Tiro.</li>
									<li>Manejo y uso de armas de fuego (DAEM).</li>
									<li>Defensa personal.</li>
									<li>Entrenamiento especial para escolta personal, personal de seguridreas afines a seguridad privada.'); ?></li>
								</ul>
							</div>
							<div class="pull-right">
								<div class="imageGalLarge">
				      				<?= $this->Html->link($this->Html->image('servicios/capacitaciones/01.jpg'),'/img/servicios/capacitaciones/01.jpg',['class'=>'group_photo','escape'=>false]); ?>
				      			</div>
				      			<div class="imageGalLarge">
					      			<?= $this->Html->link($this->Html->image('servicios/capacitaciones/02.jpg'),'/img/servicios/capacitaciones/02.jpg',['class'=>'group_photo','escape'=>false]); ?>
					      		</div>
				      			<div class="imageGalLarge">
					      			<?= $this->Html->link($this->Html->image('servicios/capacitaciones/03.jpg'),'/img/servicios/capacitaciones/03.jpg',['class'=>'group_photo','escape'=>false]); ?>
					      		</div>
				      			<div class="imageGalLarge">
					      			<?= $this->Html->link($this->Html->image('servicios/capacitaciones/04.jpg'),'/img/servicios/capacitaciones/04.jpg',['class'=>'group_photo','escape'=>false]); ?>
					      		</div>
				      			<div class="imageGalLarge">
					      			<?= $this->Html->link($this->Html->image('servicios/capacitaciones/05.jpg'),'/img/servicios/capacitaciones/05.jpg',['class'=>'group_photo','escape'=>false]); ?>
					      		</div>
				      			<div class="imageGalLarge">
					      			<?= $this->Html->link($this->Html->image('servicios/capacitaciones/06.jpg'),'/img/servicios/capacitaciones/06.jpg',['class'=>'group_photo','escape'=>false]); ?>
				      			</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<div class="Products parallax-wrapper" id="ElectronicSecurity">
	<div class="Products-services">
		<div class="container">
			<div class="row">
				<div class="col-lg-1">
					<?= $this->Html->image('icon-camara.png'); ?>
				</div>
				<div class="col-lg-5">
					<article>
						<h2>Seguridad <br>Electrónica</h2>
						<p>
						Esta parte de la seguridad es vista como el área que presta herramientas de última tecnología para ayudar a completar las otras áreas de seguridad. En  esta área se puede encontrar tanto como usted necesite, ya que hay desde un simple controlador eléctrico para picaportes que se pone en una puerta con un pulsador cualquiera, hasta un scanner ocular para control de retina color y forma de un ojo.
						</p>
					</article>
				</div>
				<div class="col-lg-6 hidden-md hidden-sm hidden-xs">
					<?= $this->Html->image('img-camara.png',['class'=>'camera']); ?>
					<?= $this->Html->image('gif-seguridad-electronica.gif',['style'=>'float:right']); ?>
				</div>
			</div>

			<section class="Products-servicesBlock">
				<div class="row">
					<div class="col-md-11 col-md-offset-1">
						<ul class="Products-servicesList">
							<li>
								<?= $this->Html->image('icon-monitoreo.png',['img-responsive']); ?>
								<div class="content">
									<span class="title">Monitoreo</span>
									<p><?= strtoupper('Brindamos el servicio integral de monitoreo por video, sensores de movimiento, infrarojos, etc. y brindamos respuesta de agentes  en tiempo real.'); ?></p>
								</div>
							</li>
							<li>
								<?= $this->Html->image('icon-suministro.png',['img-responsive']); ?>
								<div class="content">
									<span class="title">Suminstro e Instalacion</span>
									<p>Hacemos instalaciones de diversos tipos de tecnologías de seguridad, desde camras CCTV hasta puntos de acecso biometricos.</p>
								</div>
							</li>
							<li>
								<?= $this->Html->image('icon-soporte.png',['img-responsive']); ?>
								<div class="content">
									<span class="title">Soporte Técnico</span>
									<p>Brindamos el servicio de mantenimiento y configuracion de todo tipo de sistemas de seguridad.</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</section>
			<?php if (!empty($productos)): ?>
				<section class="Products-Wrapper">
					<h2 class="Product-title"><?= $productos['WpPost'][0]['post_title']; ?></h2>
					<?= $productos['WpPost'][0]['post_content']; ?>
					<?php /* ?>
					<ul class="Product-list">
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
									<?= $this->Html->image('img-placeholder-productos.jpg'); ?>
								</div>
								<div class="Product-description">
									<?= strtoupper('Honewell vista-10p') ?>
									<p>
										<?= strtoupper('panel del kit de alarma de 10 zonas') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
						<li class="Product-listItem pull-left">
							<article class="Product">
								<div class="Product-picture pull-left">
								</div>
								<div class="Product-description">
									<p>
										<?= strtoupper('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione voluptatem.') ?>
									</p>
								</div>
							</article>
						</li>
					</ul>
					<?php */ ?>
				</section>
			<?php endif ?>
		</div>
	</div>
</div>