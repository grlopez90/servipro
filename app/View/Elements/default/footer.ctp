<div class="container">
	<div class="row">
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-4 Footer-brand">
					<?= $this->Html->image('logo-footer.png'); ?>
				</div>
				<div class="col-md-8 Footer-contactInfo">
					<div class="Footer-redes">
						<ul>
							<li><?= $this->Html->image('facebook.svg',['url'=>'https://www.facebook.com/Servipronic/']); ?></li>
							<li><?= $this->Html->image('youtube.svg',['url'=>'https://www.youtube.com/channel/UCAjGvWCK8Mxk0rrcG4Fr7XA']); ?></li>
						</ul>
					</div>
					<div class="Footer-contactInfoItem">
						<ul>
							<li>Telf: (505) 2255-8000</li>
							<li>Mail: servipro@servipro.com.ni</li>
							<li>Rotonda Santo Domingo 150 varas al este. Edificio Servipro</li>
							<li>Managua, Nicaragua</li>
						</ul>
					</div>
				</div>
			</div>
			<span class="Footer-copy">
				El nombre "SERVIPRO" y su logotipo de escudo, son marcas registradas de "Servicio de Vigilancia y Protección S.A" Todos los derechos reservados 2016. Página web diseñada por CreativoCorp S.A
			</span>
		</div>
		<div class="col-md-2">
			<?= $this->Html->image('certificados-01.png'); ?>
		</div>
	</div>
</div>