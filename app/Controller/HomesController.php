<?php
App::uses('AppController', 'Controller');
/**
 * Homes Controller
 */
class HomesController extends AppController {

	public function index() {
		$this->loadModel('WpTermTaxonomy');

		$slider = $this->WpTermTaxonomy->find(
			'all',
			array(
				'conditions' => array(
					'slug' => 'slider',
				)
			)
		);

		$videos = $this->WpTermTaxonomy->find(
			'first',
			array(
				'conditions' => array(
					'slug' => 'videos-portada',
				)
			)
		);
		
		$this->WpTermTaxonomy->unbindModel(array('hasAndBelongsToMany' => array('WpPost')),true);
		$this->WpTermTaxonomy->bindModel(
			array(
				'hasAndBelongsToMany' => array(
					'WpPost' => array(
						'className' => 'WpPost',
						'joinTable' => 'wp_term_relationships',
						'foreignKey' => 'term_taxonomy_id',
						'associationForeignKey' => 'object_id',
						'unique' => 'keepExisting',
						'conditions' => '',
						'fields' => '',
						'order' => 'post_date DESC',
						'limit' => 3,
					)
				)
			)
		);
		
		$this->WpTermTaxonomy->recursive = 2;
		$articulos = $this->WpTermTaxonomy->find(
			'all',
			array(
				'conditions' => array(
					'slug' => 'articulos',
				)
			)
		);

		$this->loadModel('WpPost');
		for ($i=0; $i < 3; $i++) { 
	    	$articulos[0]['WpPost'][$i]['Image'] = $this->WpPost->find(
				'first',
				array(
					'conditions' => array(
						'WpPost.ID' => $articulos[0]['WpPost'][$i]['WpPostmetum']['meta_value'],
					),
					'fields' => array('guid')
				)
			);
	    }

		$this->set(compact(array('slider', 'videos', 'articulos')));

		$this->set('title_for_layout','Inicio');
	}

}
