<?php
App::uses('AppModel', 'Model');
/**
 * WpPostmetum Model
 *
 * @property Post $Post
 */
class WpPostmetum extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'meta_id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'meta_key';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'WpPost' => array(
			'className' => 'WpPost',
			'foreignKey' => 'post_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
