$('.Contact-btnAgent').on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	$('.Contact-bgOver').hide('slow', function() {});
	$('.Contact-btnAgentBack').show('fast', function() {});
});

$('.Contact-btnAgentBack').on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	$('.Contact-bgOver').show('slow', function() {});
	$('.Contact-btnAgentBack').hide('fast', function() {});
});

$('.tab-content').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})

$('.nav-tabsMine a').hover(function(event) {
	event.preventDefault();
	/* Act on the event */
	console.log('hoala');
	$('.triangle').hide();
	$(this).find('.triangle').show();
});

$('.tabs-left a').hover(function(event) {
	event.preventDefault();
	/* Act on the event */
	console.log('hoala');
	$('.triangle2').hide();
	$(this).find('.triangle2').show();
});

$(".group_photo").colorbox({
	rel:'group_photo', 
	transition:"elastic",
	maxWidth:"80%", 
	maxHeight:"80%",
	fixed: true,
	current: "Foto {current} de {total}"
});

$(".youtube").colorbox({
	iframe:true,
	innerWidth:720, 
	innerHeight:405}
);

$(".AboutWhoUs-listProfiles li>img").hover(function(event) {
	//console.log($(this).parent().find(".AboutWhoUs-hover"));
	$(this).parent().find(".AboutWhoUs-hover").slideDown("slow");
}, function() {
	$(".AboutWhoUs-hover").slideUp("slow");
});

$(".AboutWhoUs-hover").on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	$('.modal-gerente').html($(this).attr('data-info'));

	$('.modal-gerenteName').html($(this).attr('data-name'));
});

$(window).load(function() {
	 var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
     $('.nav-tabsMine a#'+hash).trigger('click');
     console.log($('a[href='+hash+']'));
     $('a[href="#'+hash+'"]').tab('show');
     $('.triangle').hide();
     $('a[href="#'+hash+'"]').find('.triangle').show();
     var scroll = $('a[href="#'+hash+'"]').offset().top;
     //console.log(scroll);
     $("html, body").animate({ scrollTop: scroll - 200 }, 2000);
});