<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Nuestraempresas Controller
 */
class NuestraempresaController extends AppController {

	public function index() {

		$this->loadModel('WpTermTaxonomy');
		$gerentes = $this->WpTermTaxonomy->find(
			'first',
			array(
				'conditions' => array(
					'slug' => 'gerentes',
				)
			)
		);

		$this->set(
			['gerentes' => @$gerentes,
			'title_for_layout' => 'Nuestra Empresa']
		);
		
	}

	public function contacto() {
		$Email = new CakeEmail();
		$Email->from(array($this->request->data['Contact']['Email']=>$this->request->data['Contact']['Name']))
			->emailFormat('html')
			->to(array('atencionalcliente@servipro.com.ni','servipro@servipro.com.ni'))
			->subject('Sitio Web')
			->send($this->request->data['Contact']['Message']);

		$this->redirect('/');
	}

}

//atencionalcliente@servipro.com.ni','servipro@servipro.com.ni