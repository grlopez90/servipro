<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'servipro');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('CONCATENATE_SCRIPTS', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lD v DAU`$a<b4bvG0(7{ufZg!llHJ$h=nJDX3z5/Z5C}<LZ_|0~;Nz$U/%g:qff');
define('SECURE_AUTH_KEY',  'W{CKeSYG-aDEM?N=U|$-J_%}`AKX^p_TN)2>nU1NYxfa78|WcL3i0-OWAKor~gUv');
define('LOGGED_IN_KEY',    ' A@g@:L,IXQ/rhUrfg}|&xr8UpKIF1E>Zc>>^$^-1`u+_fi{Q6S>Z8/4Isx^lqVD');
define('NONCE_KEY',        'hQ0Sn%P s`rZz;~uq`xr8ac^-l)lQiOYNr~}mK,]k2t9my<:O-0.87)C]Ji$:4:b');
define('AUTH_SALT',        'uoE)&eEk7*BFpB[mb1iS<HakVtz~1jl+4Q2.8Qn9aQM}y*OOHf!Un[%QK]TeInpb');
define('SECURE_AUTH_SALT', 'V(|.j3g$4;cP9NP<nN[f8C5WIGqnWkh};]<](b%,skB7L%mR|H=3W&()WM0:;1)t');
define('LOGGED_IN_SALT',   '7O%(,9h]a72a_Eh?P0~6J JQ;K$Ff(iwe9G>eMJ{R_!fJN,PZ_GE@8;/ltDu:2Td');
define('NONCE_SALT',       '5utdhwblDm&0NBUNMoY@:svxay~krnHA3p[ukb:isLn5M$rnG0=7E=**qMl AE=l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
